#include <stdio.h>
#include <stdlib.h> // string to double strtod

//---------------------------------
int main(int argc,char * argv[]){
//---------------------------------

  double offset = 1705085193.584;
  char itxt[80];
  sprintf(itxt,"%s","input_filename.txt");

  char otxt[80];
  sprintf(otxt,"%s","output_filename.txt");


  if(argc!=4){
    printf("Usage  : %s <input filename> <offset> <output filename>\n",argv[0]);
    printf("Example: %s %s %.3lf %s\n",argv[0],itxt,offset,otxt);
    return -1;
  }else{
    char *eptr;
    sprintf(itxt,"%s",argv[1]);
    offset = strtod(argv[2], &eptr);
    sprintf(otxt,"%s",argv[3]);
  }

  FILE * fin =fopen(itxt,"r");
  FILE * fout =fopen(otxt,"w");

  double unixtime;
  double difference;

while(fscanf(fin,"%lf\n",&unixtime)!=EOF){

  difference = unixtime-offset;
  //printf("%16.3lf %16.3lf %6.3lf\n",unixtime, offset, unixtime-offset);
  fprintf(fout,"%6.3lf\n",difference);
}

fclose(fin);
fclose(fout);
return 0;
}
