#include <stdio.h>
#include <stdlib.h> // string to double strtod

//---------------------------------
int main(int argc,char * argv[]){
//---------------------------------

  int ret=-1;
  char txt[3][80];
  float dx;  // mute variable of the integral
  float y;  // variable to be integrated
  float dy;  // variable to be integrated
  float accu=0; // result
  int  p=0; // debug

  FILE * fin0;
  FILE * fin1;
  FILE * fout;

  sprintf(txt[0],"%s","input_filename_1.pwr");
  sprintf(txt[1],"%s","input_filename_2.dt");
  sprintf(txt[2],"%s","output_filename.energy");

  if(argc!=4){
    printf("Usage  : %s <ifilename1> <ifilename2> <ofile>\n",argv[0]);
    printf("Example: %s %s %s %s\n",argv[0],txt[0],txt[1],txt[2]);
    return -1;
  }else{
    sprintf(txt[0],"%s",argv[1]);
    sprintf(txt[1],"%s",argv[2]);
    sprintf(txt[2],"%s",argv[3]);
  }

  fin0 =fopen(txt[0],"r");
  fin1 =fopen(txt[1],"r");

  while(fscanf(fin1,"%f",&dx)!=EOF){
    fscanf(fin0,"%f",&y);
    dy = dx*y;
    accu += dy;
    if(p)printf("%8.3f %8.2f %8.2f %8.2f\n",dx,y,dy,accu);
  } // end of while
  fclose(fin0);
  fclose(fin1);

  fout =fopen(txt[2],"w");
  fprintf(fout,"%f\n",accu);
  fclose(fout);

  return 0;
}
