#!/bin/bash

IDIR="./data/3955472/"
OTXT="data.txt"
if [[ $# -ne 2 ]];then
  echo "Description: GPU energy profiler"
  echo "Usage      : $0 <path to data> <outfile>"
  echo "Example    : $0 $IDIR $OTXT"
  exit 666
else
  IDIR=$1
  OTXT=$2
  rm -f $OTXT
fi

LC_ALL="C" # for awk to manage floating point numbers correctly


# check nodelist exists
printf -v NODELIST "%s/nodelist.txt" $IDIR
if [ -f "$ITXT" ]; then
echo "File $NODELIST exists"
fi


NGPU=0
echo "***************"
echo " Calculate Variations  "
echo "***************"
EXE="calculate_variations.x"
cp src/$EXE .

while read -r node ; do # Loop on nodes

  what=nvsmi
  ITXT="$node.$what.txt.good.unixtime.ready"
  if [ -f "$ITXT" ]; then
    awk '{print $2} ' $ITXT | sort | uniq >  $node.$what.multiplicity
    while read gpu; do
      let NGPU++
      echo "NODE $node GPU $gpu"
      OTXT1="$node.$what.txt.good.unixtime.ready.$gpu"
      awk -v x=$gpu '$2==x {print $0}' $ITXT > $OTXT1
      OTXT2="$node.$what.txt.good.unixtime.ready.$gpu.diff"
      ./$EXE $OTXT1 $OTXT2
      OTXT3="$node.$what.$gpu.txt"
      paste $OTXT1 $OTXT2 > $OTXT3
      rm $OTXT1
      rm $OTXT2
    done < $node.$what.multiplicity
  fi

  what=pdu
  ITXT="$node.$what.txt.good.unixtime.ready"
  if [ -f "$ITXT" ]; then
    awk '{print $2} ' $ITXT | sort | uniq >  $node.$what.multiplicity
    while read gpu; do
      let NGPU++
      echo "NODE $node PDU $gpu"
      OTXT1="$node.$what.txt.good.unixtime.ready.$gpu"
      awk -v x=$gpu '$2==x {print $0}' $ITXT > $OTXT1
      OTXT2="$node.$what.txt.good.unixtime.ready.$gpu.diff"
      ./$EXE $OTXT1 $OTXT2
      OTXT3="$node.$what.$gpu.txt"
      paste $OTXT1 $OTXT2 > $OTXT3
      rm $OTXT1
      rm $OTXT2
    done < $node.$what.multiplicity
  fi

done < $NODELIST
rm $EXE
echo N POWER SOURCES  $NGPU

echo "***************************************"
echo " Merge data in a single file"
echo "****************************************"
NGPU=0
while read -r node ; do # Loop on nodes

  if [[ $node == *"lrdn"* ]]; then
    NODEID=${node:4:4} # extract node ID = 0123 # Leonardo lrdn1234
  else
    NODEID=${node:6:2} # extract node ID = 04 # E4 acnode04
  fi

  for what in nvsmi pdu; do
    MULT=$node.$what.multiplicity
    if [ -f "$MULT" ]; then
      while read device; do
        ITXT="$node.$what.$device.txt"
        if [ -f "$ITXT" ]; then
          let NGPU++
          awk -v y=$NODEID -v x=$NGPU '{printf "%s %s %s\n",x,y,$0}' $ITXT >> $OTXT
          # the format of OTXT is "absolute GPU, node ID, local GPU, timestamps,... and all other nvsmi variables"
        fi
        rm $ITXT
      done < $MULT
    fi
  done
done < $NODELIST

rm *.multiplicity
rm *.datetime
rm *.unixtime
rm *.good
rm *.ready

mv offset.t0.txt t0.txt

#eof
