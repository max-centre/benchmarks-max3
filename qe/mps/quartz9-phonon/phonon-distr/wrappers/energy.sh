#!/bin/bash

ITXT="data.txt"
if [[ $# -ne 1 ]];then
  echo "Description: Calculate energy consumption of the pre-processed job"
  echo "Usage      : $0 <infile>"
  echo "Example    : $0 $ITXT"
  exit 666
else
  ITXT=$1
fi


###############
# PRELIMINARY #
###############

# grab the executable
EXE="integral.x"
cp src/$EXE .

# create gpu list and node list
LISTGPU="gpu.list"
TMP="tmp"
LISTNODE="node.list"
awk '{print $1,$2,$4}' data.txt | sort | uniq > $LISTGPU
awk '{print $2}' data.txt | sort | uniq > $LISTNODE


#######
# GPU #
#######
OTXT="egpu.txt"
rm -rf $OTXT
printf "#%6s %6s %6s %12s \n" absgpu node gpu joule >> $OTXT # header
while read -r absgpu node gpu
do
  awk -v a=$node -v b=$gpu '$2==a && $4==b {print $5,$13}' $ITXT > $TMP
  energy=`./integral.x $TMP`
  printf "%7s %6s %6s %12s \n" $absgpu $node $gpu $energy >> $OTXT
done < $LISTGPU


########
# NODE #
########
ITXT="egpu.txt"
OTXT="enode.txt"
rm -rf $OTXT

printf "#%6s %12s \n" node joule >> $OTXT

while read -r node
do
  awk '/^[^#]/ { print $0 }' $ITXT > $TMP  # remove header line(s) identified with #
  energy=`LC_ALL="C"  awk -v x=$node  '$2==x {sum +=$4} END {printf "%.2f", sum}' $TMP` # sum up the energy consumed by each gpu to get the energy consumed by a node
  printf "%7s %12s \n" $node $energy >> $OTXT
done < $LISTNODE



#######
# JOB #
#######
ITXT="egpu.txt"
OTXT="ejob.txt"
rm -rf $OTXT

printf "#%11s\n" joule >> $OTXT

awk '/^[^#]/ { print $0 }' $ITXT > $TMP  # remove header line(s) identified with #
energy=`LC_ALL="C"  awk '{sum +=$4} END {printf "%.2f", sum}' $TMP` # sum up the energy consumed by each gpu to get the energy consumed by a node
printf "%12s \n" $energy >> $OTXT




############
# CLEAN UP #
############

rm $EXE
rm $LISTGPU
rm $LISTNODE
rm $TMP

#eof
