#!/bin/bash
# start nvidia-smi on all nodes
dir=${PWD} # for ssh nvidia-smi

# Export nodelist
rm -rf nodelist.txt
for node in `scontrol show hostname`; do
  echo $node >> nodelist.txt
done

for node in `scontrol show hostname`; do
  exe="wrappers/nvsmi/nvsmi_start.sh"
  ssh $node "cd $dir && $exe" &
done

