#!/bin/bash

IDIR="./data/3955472/"
IDIR="../raw/bgk3d/25130/"

if [[ $# -ne 1 ]];then
  echo "Description: GPU profile analysis"
  echo "Note       : Supports multi-GPU and multi-node as well as data from PDU"
  echo "Usage      : $0 <path to data>"
  echo "Example 1  : $0 $IDIR"
  echo "Example 2  : $0 /home/matt/matteo/work/git/energpu/data/3944035/"
  exit 666
else
  IDIR=$1
fi

# First part is in Bash only
#./metadata.sh $IDIR          # get job info
./formal_check.sh $IDIR      # time series are formatted as expected

# compile source files
./compile.sh

# Post processing uses C/C++ and bash
TXT="data.txt"
./timestamp_and_t0.sh $IDIR
./variation.sh $IDIR $TXT
./energy.sh $TXT

# Analisis in C/C++/ROOT
#./ingest.sh


#exit

#./getEnergyinJoule.sh $IDIR
#./genReport.sh $IDIR
