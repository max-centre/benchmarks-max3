#include <stdio.h>
#include <stdlib.h> // string to double strtod

//---------------------------------
float main(int argc,char * argv[]){
//---------------------------------

  int  p=0; // debug

  int ret=-1;
  char txt[80];
  float dx;  // mute variable of the integral
  float y;  // variable to be integrated
  float dy;  // variable to be integrated
  float accu=0; // result

  FILE * fin;

  sprintf(txt,"%s","input_twocolumn.txt");

  if(argc!=2){
    printf("Usage  : %s <ifilename> \n",argv[0]);
    printf("Example: %s %s \n",argv[0],txt);
    return -1;
  }else{
    sprintf(txt,"%s",argv[1]);
  }

  fin =fopen(txt,"r");

  while(fscanf(fin,"%f",&dx)!=EOF){
    fscanf(fin,"%f",&y);
    dy = dx*y;
    accu += dy;
    if(p)printf("%8.3f %8.2f %8.2f %8.2f\n",dx,y,dy,accu);
  } // end of while
  fclose(fin);


  fprintf(stdout,"%8.2f\n",accu);

  return 0;
}
