# MPS for small workloads

This folder contains test cases for MPS and comparison with standard distributed runs of QE

- quartzs-pwscf: scf simulation with MPS and distributed for 3 systems of SiO2 with increasing atom number (9,18,36)
- quartz9-phonon: linear dispersion for SiO2 with 9 atoms with MPS and distributed
- nodes2: example of multi-node job with MPS.
