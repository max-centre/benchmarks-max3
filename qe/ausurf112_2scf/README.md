### Ausurf


### Platforms

- marenostrum5 : 2x Intel Sapphire Rapids 8460Y+ at 2.3Ghz and 32c each + 4x Nvidia Hopper GPUs with 64 HBM2 memory ; documentation [here](https://www.bsc.es/supportkc/docs/MareNostrum5/overview)
- davinci1 : 2x AMD EPYC 7402 24-Core CPUs + 4x NVIDIA A100 GPU (40 Gb)
