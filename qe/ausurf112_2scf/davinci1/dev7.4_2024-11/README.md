### Single-node Ausurf 2-steps simulation on DaVinci1 @ Leonardo S.p.A.

This test shows the scalability of Ausurf with RG parallelization with awareness

Number of steps: 2

The folder contains

* inputfiles: the input files, *.UPF and *.in 
* result.dat the results as generated from JUBE
* logifiles/000000 the JUBE directory with application logfiles  

Develop GPU version from QE/7.4

Installed with the following software stack

* nvhpc/21.9
* cuda/11.4
* openmpi/3.1.5
* fftw/3.3.10
* mkl/2022.0.1

