### Ausurf simulation on Leonardo booster

This test shows the scalability of Ausurf with RG parallelization with awareness

The folder contains

* the input files, *.UPF and *.in 
* result*.dat the results as generated from JUBE
* *.xml the xml files with parameter concretizations
* 000000 the JUBE directory with application logfiles  

Develop GPU version from develop\_omp5 based on QE/7.2

Installed with the following software stack

* cce/14.0.2 
* cray-fftw/3.3.10.1 
* cray-libsci/22.08.1.1
* rocm/5.3.3
* cray-mpich/8.1.27

