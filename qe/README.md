### QuantumESPRESSO workloads

| name         | CODE  | pseudo      | electrons | nbands   | FFT dimension | G-vectors | k-points   | irrep  |
| ----------   | ----- | --------    | --------- | -------- | ------------- | --------- | ---------- | ------ |
| ausurf       | PWSCF | US + cc     | 1232      | 800      | 125,  64, 200 | 763307    | 2	     | -      |
| si16l        | PH    | NC + cc     | 256  	 | 154      | 80,   80, 256 | 665603    | 128 (ph)   | 192    | 
| sio2\_576    | PWSCF | US + cc     | 3072      | 1536     | 150, 135, 180 | 1668577   | 1          | -      |
| water (small)| PWSCF | US/PAW + cc | 800       | 400      | 180, 180, 180 | 1522006   | 1          | -      |
| water (large)| PWSCF | US/PAW + cc | 3178      | 1884     | 320, 320, 320 | 7854972   | 1          | -      |

