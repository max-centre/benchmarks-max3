#!/bin/bash
export CUDA_MPS_LOG_DIRECTORY=./pipe${SLURM_NODEID}
export CUDA_MPS_PIPE_DIRECTORY=./pipe${SLURM_NODEID}
if [ $SLURM_LOCALID -eq 0 ]; then
    nvidia-cuda-mps-control -d
fi
ph.x -nk $NPOOLS -ni $NIMAGES -i ph.in > out.0_0
