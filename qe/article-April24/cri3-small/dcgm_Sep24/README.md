### CRI3 - 480 atoms

FFT dimensions: ( 120, 192, 640)

number of k-points: 3

number of bands: 1944

simulation: 1 scf step for ground state (no convergence)

QE version 7.3.1

Partition DCGP : dual socket 56 cores Intel Sapphire Rapids CPU ( Intel Xeon Platinum 8480p, 2.00 GHz TDP 350W )

Intel oneapi software stack:

- intel-oneapi-mpi/2021.10.0
- intel-oneapi-tbb/2021.10.0
- intel-oneapi-mkl/2023.2.0--intel-oneapi-mpi--2021.10.0-scalapack-openmp-qkfqalj
- libxc/5.2.3--intel--2021.10.0-hf2rg24
- quantum-espresso/7.3.1--intel-oneapi-mpi--2021.10.0--intel--2021.10.0-mkl

