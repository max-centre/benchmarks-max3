## Full SCF of Carbon Nanotube + Porphyrins


* Large test case simulated with 8 nodes on leonardo_boost@CINECA
* 3139 bands and 2.4 X 10^6 plane waves
* KPI 1.1:
    * 7.2 (march 2023) t.t.s = 1576 secs
    * 7.3 (december 2023) t.t.s = 936 secs
    * 7.4 (november 2024) t.t.s.= 915 secs
    * improvements:
        * acceleration of mix\_rho  after 7.2 from 600 to 30 secs 
        * optimization of data locality and data movement 
        * total improvement > 40 %   
