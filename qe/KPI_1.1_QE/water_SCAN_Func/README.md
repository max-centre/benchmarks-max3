## 64 molecules' water clusted with SCAN functional

* small test for single node performance tested on leonardo_boost@CINECA
* 256 bands and 4.5X10^6 plane waves
* metaGGA SCAN Functional taken from libxc, in order perform the test it is necessary to compile the program 
  linking with libxc v> 5.0 compatible only with qe version > 7.3 
* KPI1.1:
    * 7.3 (december 23) 1236 secs
    * 7.3.1 (april 24)   325 secs 
    * 7.4 ( november 24) 208 secs 
    * improvement w.r.t baseline 83 % 
    * relevant development work:
        * acceleration of metaGGA exchange and correlation kernels
        * optimization of data movement and data locality on NVidia GPUs
    
