### Ausurf


### Platforms

- leonardo booster : Intel Xeon CPU + 4 NVIDIA A100 GPUs (64GB) ; documentation [here](https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.2.1%3A+LEONARDO+Booster+UserGuide)
- davinci1 : 2x AMD EPYC 7402 24-Core CPUs + 4x NVIDIA A100 GPU (40 Gb)
