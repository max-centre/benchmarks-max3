Covid Spike Protein - Weak Scaling on diagonalization workload

Tags used in this benchmark:
    * marenostrum
    * gpu

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       EB/apps libtool Autoconf Python
         ScaLAPACK/2.2.0-gompi-2023b-fb FFTW/3.3.10-GCC-13.2.0 CUDA/12.5.0
         netCDF-Fortran GCC/12.3.0
         CMake/3.26.3-GCCcore-12.3.0 Ninja/1.11.1-GCCcore-12.3.0
