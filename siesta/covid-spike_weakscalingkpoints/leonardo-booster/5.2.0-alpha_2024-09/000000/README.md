Covid Spike Protein - Weak Scaling on number of K-Points

Tags used in this benchmark:
    * leonardo
    * gpu

Files included (apart from JUBE-related files) include:
    * Input fdf files.
    * SIESTA general output file (*.out)
    * Submission script (submit.job)
    * Slurm output files (*.out, *.err), submission stderr, stdout.
    * Timings in *.times and time.json files.
    * Memory allocation info in *.alloc.


SIESTA was compiled by loading the following modules:
       gcc/12.2.0 fftw/3.3.10--openmpi--4.1.6--gcc--12.2.0 openblas/0.3.24--gcc--12.2.0 netlib-scalapack/2.2.0--openmpi--4.1.6--gcc--12.2.0
         netcdf-fortran/4.6.1--openmpi--4.1.6--gcc--12.2.0 cuda/12.1
         cmake ninja
