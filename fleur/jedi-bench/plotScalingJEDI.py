# Import relevant parts of Matplotlib
import matplotlib.pyplot as plt
import numpy as np

x1, y1 = [], []
for line in open('resultsTiny.txt', 'r'):
   values = [float(s) for s in line.split()]
   x1.append(values[0])
   y1.append(values[1])

refTime = y1[0]
y1b = []
for value in y1:
   y1b.append(refTime / value)

x2, y2 = [], []
for line in open('resultsTinyJEDI.txt', 'r'):
   values = [float(s) for s in line.split()]
   x2.append(values[0])
   y2.append(values[1])

refTime = y2[0]
y2b = []
for value in y2:
   y2b.append(refTime / value)


# Create figure with labels for the plot
fig, (ax1) = plt.subplots(1,1)

ax1.set_xlabel('number of nodes')
ax1.set_ylabel('speedup')

# Plot the data
leg0 = ax1.plot([x1[0], x1[3]], [y1b[0], y1b[0]*(x1[3]/x1[0])], color='lightgray', linestyle='-', linewidth=2,label='ideal scaling')
leg1 = ax1.plot(x1, y1b,'ro-',label='A100')
leg2 = ax1.plot(x2, y2b,'bo-',label='GH200')
#leg3 = ax1.plot(x3, y3b,'mo-',label='JEDI')
#ax1.legend(handles=[leg0, leg1, leg2])
ax1.legend(["ideal scaling", "A100", "GH200"])
ax1.set_title("FLEUR on Grace Hopper")
ax1.text(1.0,13.8,"reference time A100:\n\n    $1691.45~\mathrm{s}$")
ax1.text(1.0,10.3,"reference time GH200:\n\n    $856.88~\mathrm{s}$")
#ax1.text(1.0,6.8,"reference time JEDI-08-06:\n\n    $600.33~\mathrm{s}$")
xticks = np.arange(0, 16, 1)

# Save the plot to the file 'spinSpiral.png' 
plt.savefig('benchResults-FLEUR-JEDI.png',dpi=300)

# Also irectly display the plot
plt.show()

