

def run_bigdft(name, posinp, **kwargs):
    """The basic function to run a calculation.

    This function executes the SystemCalculator class with a provided keyword arguments.
    It creates a tarfile which includes the logfile of the run as well as
    the time.yaml performance file associated.

    Args:
        posinp (path): the posinp file
        name (str): the name of the run
        **kwargs: a dictionary following the same API of SystemCalculator class

    Returns:
        The total estimated memory of the calculation in the case of a `dry_run`,
        otherwise the total energy.
    """
    from BigDFT.Calculators import SystemCalculator
    from os.path import join, isfile
    from futile.Utils import create_tarball
    code = SystemCalculator()
    log = code.run(name=name, posinp=posinp, **kwargs)
    jobname = name
    target = jobname+'.tar.gz'
    dry_run = kwargs.get('dry_run', False)
    files = ['log-' + jobname + '.yaml']
    if dry_run:
        result = log.log['Estimated Memory Peak (MB)']
        print(log.log['Total Number of Orbitals'], result)
    else:     
        result = log.energy
        print(result)
        for timefile in [join('data-'+jobname+',time-'+jobname+'.yaml'),'time-'+jobname+'.yaml']:
            if isfile(timefile):
                files += [timefile]
                break
    create_tarball(target, files=files)
    return result


def campaign(run_spec,**kwargs):
    """
    Create a dictionary of runs to be passed to a Remotemanager Dataset
    for a set of benchmark specifications.
    
    Args:
        run_spec (dict): dictionary of the runs organized as {filepath: spec}
           where **spec is the set of keyword arguments which have to be passed
           to the `append_run` method of the dataset.
           `filepath` should eb the relative path of the input file to be executed.
        **kwargs: extra arguments to be given to the `run_bigdft` function.

    Returns:
        dict: the dictionary of arguments to be given to the creation of the RemoteManager dataset.

    """
    from os.path import basename
    runner_args = []
    for file, runs in run_spec.items():
        filename = basename(file)
        for run in runs:
            func_args = {'posinp': filename}
            name = run['name']
            func_args['name'] = name
            func_args.update(**kwargs)
            #print(file, run)
            args = dict(extra_files_send=[file],
                        extra_files_recv=[name + '.tar.gz'],
                        jobname=name,
                        arguments=func_args, **run)
            runner_args.append(args)

    return runner_args


def find_timefile(log, timefile):
    from os.path import isfile, dirname, basename, join
    from os import system
    from itertools import product
    if timefile is not None:
        return timefile
    radical = log.log.get('radical','')
    logname = basename(log.label)
    basedir = dirname(log.label)
    datadir = log.data_directory
    run_datadir = log.datadir
    possible_dirs = [run_datadir, join(basedir, datadir)]
    possible_radicals = [radical, logname.replace('log-', '').replace('.yaml', '')]
    for directory, rad in product(possible_dirs, possible_radicals):
        timefile = join(directory, 'time' + ('-'+rad if len(rad) > 0 else '') + '.yaml')
        if isfile(timefile):
            # to solve yaml compliancy for old runs
            system("sed -i s/^\ *\:\ null/\ \ \ \ null/g "+timefile)
            return timefile


def log_performance_info(log):
    from numpy import nan
    ks = {'Hostname': 'Root process Hostname',
          'Date': 'Timestamp of this run',
          'MPI':'Number of MPI tasks',
          'OMP': 'Maximal OpenMP threads per MPI task',
          'Mem': 'Memory Consumption Report',
          'Walltime': 'Walltime since initialization',
          'SF': 'Total No. Support Functions',
          'Orbitals': 'Total Number of Orbitals',
          'Electrons': 'Total Number of Electrons'
          }
    df = {k: log.log.get(v,nan) for k,v in ks.items()}
    if ks['Mem'] in log.log:
        df['Mem'] = float(df['Mem']['Memory occupation']['Memory Peak of process'].rstrip('MB'))
    df['Nat'] = len(log.astruct['positions'])
    df['cores']=df['MPI']*df['OMP']
    df['TotMem']=df['MPI']*df['Mem']
    df['NodeMem']=df.get('MPI tasks of root process node', 1)*df['Mem']
    df['CPUhours']=df['cores']*df['Walltime']/3600.0
    df['CPUmin/at']=df['CPUhours']*60.0/df['Nat']
    df['Memory/at']=df['TotMem']/df['Nat']
    return df


def log_spec(filename, timefile=None, log=None):
    from BigDFT.Logfiles import Logfile
    from futile import YamlIO
    if log is None:
        log = Logfile(filename)
    timefile = find_timefile(log, timefile)
    wfn_opt = None
    if timefile is not None:
        dt = YamlIO.load(timefile)[-1]
        if dt is not None:
            wfn_opt = dt.get('SUMMARY',{}).get('WFN_OPT',[None, None])[1]

    data = {'name': log.log['radical'], 'timefile': timefile, 'WFN_OPT': wfn_opt}
    if hasattr(log, 'energy'):
        data['energy'] = log.energy
    data.update(log_performance_info(log))
    return data


def df_select(df, select_dict):
    dft = df
    for column, pattern in select_dict.items():
        if isinstance(column, int):
            dft = dft.filter(like=pattern, axis=column)
            continue
        if isinstance(pattern, str):
            dft = dft[dft[column].str.contains(pattern)]
        else:
            dft = dft[dft[column] == pattern]
    return dft


def extract_results(directory):
    from futile.Utils import file_list
    import tarfile
    from os.path import join
    from os import system
    for archive in file_list(directory,suffix='.tar.gz',
                             exclude='files',  # old RemoteRunner
                             include_directory_path=True):
        arch = tarfile.open(archive)
        arch.extractall(path=directory)
        arch.close()
    return get_logfile_list(directory)


def get_logfile_list(directory):
    from futile.Utils import file_list
    # this should be modified to recursively check the log in the subdirectories
    return file_list(directory=directory,prefix='log-',
                     suffix='.yaml',include_directory_path=True)


def dataframe(allogs):
    from pandas import DataFrame
    fulldata = {}
    for logfile in allogs:
        try:
            fulldata[logfile] = log_spec(logfile)
        except Exception as e:
            print(logfile, str(e))
    df = DataFrame(fulldata).T
    return df


def draw_barplot(ids,data,width=0.8,shift=0,x=None,ax=None,
                 labels=None, annotations=None, aggregate=None, colors=None):
    from matplotlib import pyplot as plt
    from futile.Time import aggregate_names
    prop_cycle = plt.rcParams['axes.prop_cycle']
    if colors is None:
        colors = prop_cycle.by_key()['color']
    if ax is None:
        fig, ax = plt.subplots()
    bottom=0
    n=0
    dt = data[0] if aggregate is None else aggregate_names(data[0],aggregate)
    for i, (cat, vals) in enumerate(dt):
        if x is None:
            n = max(n, len(vals))
            x = [t for t in range(n)]            
        xs = [t + shift for t in x]
        bar=ax.bar(xs,vals,label=cat,bottom=bottom,width=width,color=colors[i%len(colors)])
        bottom += vals
    ax.set_xticks(x,ids)
    if labels is not None:
        plt.bar_label(bar,labels=labels if labels is not True else None)
    if annotations is not None:
        for lb,xt,yt in zip(annotations,xs,data[1]):
            ax.annotate(lb,(xt,yt),ha='center',va='bottom')#,rotation='vertical')
    return ax


def add_label_band(ax, axis, end, start, label, *, spine_pos=-0.05, tip_pos=-0.02):
    """
    Helper function to add bracket around y-tick labels.

    Adapted from ``https://stackoverflow.com/questions/67235301/vertical-grouping-of-labels-with-brackets-on-matplotlib``

    Parameters
    ----------
    ax : matplotlib.Axes
        The axes to add the bracket to

    axis : str
        The axis of the axes, x or y

    end, start : floats
        The positions in *data* space to bracket on the y-axis

    label : str
        The label to add to the bracket

    spine_pos, tip_pos : float, optional
        The position in *axes fraction* of the spine and tips of the bracket.
        These will typically be negative

    Returns
    -------
    bracket : matplotlib.patches.PathPatch
        The "bracket" Aritst.  Modify this Artist to change the color etc of
        the bracket from the defaults.

    txt : matplotlib.text.Text
        The label Artist.  Modify this to change the color etc of the label
        from the defaults.

    """
    import matplotlib.path as mpath
    import matplotlib.patches as mpatches

    mean = (end+start) / 2
    if axis == 'y':
        # grab the yaxis blended transform
        transform = ax.get_yaxis_transform()
        shape = [[tip_pos, end],
                [spine_pos, end],
                [spine_pos, start],
                [tip_pos, start],]
        rotation = 'vertical'
        xtext = spine_pos
        ytext = mean
        ha="right"
        va="center"        
    if axis == 'x':
        transform = ax.get_xaxis_transform()
        shape = [[end, tip_pos],
                [end, spine_pos],
                [start, spine_pos],
                [start, tip_pos],]
        rotation = 'horizontal'
        xtext = mean
        ytext = spine_pos-0.01
        va="top"
        ha="center"
        


        # add the bracket
    bracket = mpatches.PathPatch(
        mpath.Path(shape),
        transform=transform,
        clip_on=False,
        facecolor="none",
        edgecolor="k",
        linewidth=2,
    )
    ax.add_artist(bracket)

    # add the label
    txt = ax.text(
        xtext,
        ytext,
        label,
        ha=ha,va=va,
        rotation=rotation,
        clip_on=False,
        transform=transform,
    )

    return bracket, txt


def calculate_nodes(df, cpus_per_node):
    df['Nodes']=df['cores']/cpus_per_node
    df['SF/Node']=df['SF']/df['Nodes']
    df['Orbitals/Node']=df['Orbitals']/df['Nodes']


def minimum_dataframe(df,cpus_per_node,
                      walltime_key='Walltime',
                      groups=['Nat','Nodes']):
    """Find the minimum walltime upon runs having the same number of nodes"""
    mins=[]
    if 'Nodes' not in df.columns:
        calculate_nodes(df, cpus_per_node)
    for group,dft in df.groupby(groups):
        #print(nat,nodes)
        lowest=dft.index[dft[walltime_key].to_numpy().argmin()]
        #print(lowest)
        mins.append(lowest)
    return df.T[mins].T