# GrCo GW workflow with Yambo

Calculation of quasi-particle corrections on a graphene/Co interface (GrCo) composed by a graphene sheet adsorbed on a Co slab 4 layer thick, also including a vacuum layer as large as the Co slab. The test represents a prototype calculation, as it involves the evaluation of the response function, of the Hartree-Fock self-energy and, finally, of the correlation part of the self-energy.

The strong scaling tests start from a ground-state calculation (performed with Quantum ESPRESSO) using a 24x24x1 k-point grid, resulting in a total of 61 irreducible k-points. The GW workflow was done in Plasmon Pole Approximation (PPA), the number of bands used was 2000 and the energy cutoff on the irreducible response function (Xo) set to 20 Ry (both production values) in order to make the test meaningful for scalability assessment.
